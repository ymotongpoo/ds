.. -*- coding: utf-8 -*-
   Date: Wed Feb 13 23:34:10 2013
   Author: ymotongpoo (Yoshifumi YAMAGUCHI, ymotongpoo AT gmail.com)

.. _README:

====================================
 Data structure and algorithm in Go
====================================

.. image:: https://drone.io/bitbucket.org/ymotongpoo/ds/status.png

This repository is basic learnings of standard data structures and algorithms.
All codes are under New BSD license.
