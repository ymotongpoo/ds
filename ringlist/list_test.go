package ringlist

import (
	"reflect"
	"testing"
)

func TestListInsertFirst(t *testing.T) {
	Input := []*Element{
		&Element{Value: 1},
		&Element{Value: 2},
		&Element{Value: 3},
	}
	Output1 := []int{3, 2, 1}
	Output2 := []int{1, 2, 3}

	l := NewList()
	for _, e := range Input {
		l.InsertFirst(e)
	}

	out1 := make([]int, len(Input))
	out2 := make([]int, len(Input))
	e1 := l.First()
	e2 := l.Last()
	for i := 0; i < l.Length; i++ {
		out1[i] = e1.Value
		out2[i] = e2.Value
		e1 = e1.Next()
		e2 = e2.Prev()
	}

	if !reflect.DeepEqual(out1, Output1) {
		t.Errorf("forward: out=%v, want %v\n", out1, Output1)
	}
	if !reflect.DeepEqual(out2, Output2) {
		t.Errorf("backward: out=%v, want %v\n", out2, Output2)
	}
	if l.Length != len(Input) {
		t.Errorf("out=%v, want %v\n", l.Length, len(Input))
	}
	if l.first.prev != l.last {
		t.Errorf("edge is not linked each other")
	}
	if l.last.next != l.first {
		t.Errorf("edge is not linked each other")
	}
}

func TestListInsertLast(t *testing.T) {
	Input := []*Element{
		&Element{Value: 1},
		&Element{Value: 2},
		&Element{Value: 3},
	}
	Output1 := []int{1, 2, 3}
	Output2 := []int{3, 2, 1}

	l := NewList()
	for _, e := range Input {
		l.InsertLast(e)
	}

	out1 := make([]int, len(Input))
	out2 := make([]int, len(Input))
	e1 := l.First()
	e2 := l.Last()
	for i := 0; i < l.Length; i++ {
		out1[i] = e1.Value
		out2[i] = e2.Value
		e1 = e1.Next()
		e2 = e2.Prev()
	}

	if !reflect.DeepEqual(out1, Output1) {
		t.Errorf("forward: out=%v, want %v\n", out1, Output1)
	}
	if !reflect.DeepEqual(out2, Output2) {
		t.Errorf("backward: out=%v, want %v\n", out2, Output2)
	}
	if l.Length != len(Input) {
		t.Errorf("out=%v, want %v\n", l.Length, len(Input))
	}
	if l.first.prev != l.last {
		t.Errorf("edge is not linked each other")
	}
	if l.last.next != l.first {
		t.Errorf("edge is not linked each other")
	}
}

func TestListDeleteFirst(t *testing.T) {
	Input := []*Element{
		&Element{Value: 1},
		&Element{Value: 2},
		&Element{Value: 3},
	}
	Input2 := []*Element{
		&Element{Value: 100},
		&Element{Value: 200},
		&Element{Value: 300},
	}
	Output := []int{100, 200, 300}
	l := NewList()

	for _, e := range Input {
		l.InsertLast(e)
	}
	for _, e := range Input2 {
		l.InsertLast(e)
	}
	for i := 0; i < len(Input); i++ {
		l.DeleteFirst()
	}

	out := make([]int, len(Input2))
	e := l.First()
	for i := 0; i < l.Length; i++ {
		out[i] = e.Value
		e = e.Next()
	}

	if !reflect.DeepEqual(out, Output) {
		t.Errorf("out=%v, want %v\n", out, Output)
	}
	if l.Length != len(Input2) {
		t.Errorf("out=%v, want %v\n", l.Length, len(Input2))
	}
	if l.first.prev != l.last {
		t.Errorf("edge is not linked each other")
	}
	if l.last.next != l.first {
		t.Errorf("edge is not linked each other")
	}
}

func TestListDeleteLast(t *testing.T) {
	Input := []*Element{
		&Element{Value: 1},
		&Element{Value: 2},
		&Element{Value: 3},
	}
	Input2 := []*Element{
		&Element{Value: 100},
		&Element{Value: 200},
		&Element{Value: 300},
	}
	Output := []int{1, 2, 3}

	l := NewList()
	for _, e := range Input {
		l.InsertLast(e)
	}
	for _, e := range Input2 {
		l.InsertLast(e)
	}
	for i := 0; i < len(Input); i++ {
		l.DeleteLast()
	}

	out := make([]int, len(Input))
	e := l.First()
	for i := 0; i < l.Length; i++ {
		out[i] = e.Value
		e = e.Next()
	}

	if !reflect.DeepEqual(out, Output) {
		t.Errorf("out=%v, want %v\n", out, Output)
	}
	if l.Length != len(Input) {
		t.Errorf("out=%v, want %v\n", l.Length, len(Input))
	}
	if l.first.prev != l.last {
		t.Errorf("edge is not linked each other")
	}
	if l.last.next != l.first {
		t.Errorf("edge is not linked each other")
	}
}

func TestListInsertAt(t *testing.T) {
	Input := []*Element{
		&Element{Value: 1},
		&Element{Value: 2},
		&Element{Value: 3},
		&Element{Value: 4},
		&Element{Value: 5},
	}
	Output := []int{1, 2, 3, 100, 4, 5}
	l := NewList()

	for _, e := range Input {
		l.InsertLast(e)
	}
	l.InsertAt(3, &Element{Value: 100})
	out := make([]int, len(Input)+1)

	e := l.First()
	for i := 0; i < l.Length; i++ {
		out[i] = e.Value
		e = e.Next()
	}

	if !reflect.DeepEqual(out, Output) {
		t.Errorf("out=%v, want %v\n", out, Output)
	}
	if l.Length != len(Input)+1 {
		t.Errorf("out=%v, want %v\n", l.Length, len(Input)+1)
	}
}

func TestListDeleteAt(t *testing.T) {
	Input := []*Element{
		&Element{Value: 1},
		&Element{Value: 2},
		&Element{Value: 3},
		&Element{Value: 100},
		&Element{Value: 4},
		&Element{Value: 5},
	}
	Output := []int{1, 2, 3, 4, 5}
	l := NewList()

	for _, e := range Input {
		l.InsertLast(e)
	}
	l.DeleteAt(3)
	out := make([]int, len(Input)-1)

	e := l.First()
	for i := 0; i < l.Length; i++ {
		out[i] = e.Value
		e = e.Next()
	}

	if !reflect.DeepEqual(out, Output) {
		t.Errorf("out=%v, want %v\n", out, Output)
	}
	if l.Length != len(Input)-1 {
		t.Errorf("out=%v, want %v\n", l.Length, len(Input)-1)
	}
}
