package ringlist

type List struct {
	first  *Element
	last   *Element
	Length int
}

type Element struct {
	next  *Element
	prev  *Element
	Value int
}

func (e *Element) Next() *Element {
	return e.next
}

func (e *Element) Prev() *Element {
	return e.prev
}

func NewList() *List {
	l := &List{}
	l.first = l.last
	return l
}

func (l *List) First() *Element {
	return l.first
}

func (l *List) Last() *Element {
	return l.last
}

func (l *List) InsertFirst(e *Element) *Element {
	l.Length++
	if l.first == nil {
		l.first = e
		l.last = l.first
		e.next = e
		e.prev = e
	} else {
		e.next = l.first
		e.prev = l.last
		l.first.prev = e
		l.last.next = e
		l.first = e
	}
	return e.next
}

func (l *List) InsertLast(e *Element) *Element {
	l.Length++
	if l.last == nil {
		l.last = e
		l.first = l.last
		e.next = e
		e.prev = e
	} else {
		e.next = l.first
		e.prev = l.last
		l.first.prev = e
		l.last.next = e
		l.last = e
	}
	return e
}

func (l *List) DeleteFirst() *Element {
	ret := l.first
	if l.first == nil {
		return nil
	}
	if l.first == l.last {
		l.first = nil
		l.last = nil
	}
	l.last.next = l.first.next
	l.first.next.prev = l.last
	l.first = l.first.next
	l.Length--
	return ret
}

func (l *List) DeleteLast() *Element {
	ret := l.last
	if l.last == nil {
		return nil
	}
	if l.last == l.first {
		l.first = nil
		l.last = nil
	}
	l.last.prev = l.first.prev
	l.last.prev.next = l.first
	l.last = l.last.prev
	l.Length--
	return ret
}

func (l *List) InsertAt(n int, e *Element) *Element {
	cur := l.first
	if n >= 0 {
		if n%l.Length == 0 {
			return l.InsertFirst(e)
		} else if n%l.Length == l.Length-1 {
			return l.InsertLast(e)
		}
		for n > 0 {
			cur = cur.next
			n--
		}
	} else {
		if n%l.Length == -1 {
			return l.InsertLast(e)
		} else if n%l.Length == 0 {
			return l.InsertFirst(e)
		}
		for n < 0 {
			cur = cur.next
			n++
		}
	}
	l.Length++
	e.prev = cur.prev
	cur.prev.next = e
	e.next = cur
	cur.prev = e
	return e
}

func (l *List) DeleteAt(n int) *Element {
	cur := l.first
	if n > 0 {
		if n%l.Length == 0 {
			return l.DeleteFirst()
		} else if n%l.Length == l.Length-1 {
			return l.DeleteLast()
		}
		for n > 0 {
			cur = cur.next
			n--
		}
	} else {
		if n%l.Length == -1 {
			return l.DeleteLast()
		} else if n%l.Length == 0 {
			return l.DeleteFirst()
		}
		for n < 0 {
			cur = cur.next
			n++
		}
	}
	l.Length--
	cur.prev.next = cur.next
	cur.next.prev = cur.prev
	return cur
}
