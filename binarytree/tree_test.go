package binarytree

import (
	"reflect"
	"testing"
)

var Input = []int{4, 2, 6, 1, 3, 5, 7}

var Output = &Node{
	Value: 4,
	left: &Node{
		Value: 2,
		left: &Node{
			Value: 1,
		},
		right: &Node{
			Value: 3,
		},
	},
	right: &Node{
		Value: 6,
		left: &Node{
			Value: 5,
		},
		right: &Node{
			Value: 7,
		},
	},
}

func TestAdd(t *testing.T) {
	out := NewNode(Input[0])
	for v := range Input[1:] {
		out.Add(v)
	}

	if reflect.DeepEqual(out, Output) {
		t.Errorf("out=%v, want=%v", out, Output)
	}
}
