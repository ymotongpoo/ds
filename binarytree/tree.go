package binarytree

// Node express binary tree structure.
type Node struct {
	parent *Node // Pointer to parent node
	left  *Node  // Pointer to left node
	right *Node  // Pointer to right node
	Value int
}

// NewNode returns pointer to a new node.
func NewNode(value int) *Node {
	return &Node{
		Value: value,
	}
}

// Left returns left child node of n.
func (n *Node) Left() *Node {
	return n.left
}

// Right returns right child node of n.
func (n *Node) Right() *Node {
	return n.right
}

// Parent returns parent node of n.
func (n *Node) Parent() *Node {
	return n.parent
}

// Add returns a pointer to node where value is stored into.
func (n *Node) Add(value int) *Node {
	var node *Node
	if value > n.Value {
		node = n.right
	} else {
		node = n.left
	}
	if node != nil {
		return node.Add(value)
	}
	node = NewNode(value)
	node.parent = n
	return node
}

// Search returns a pointer to node where value is.
func (n *Node) Search(value int) *Node {
	if value > n.Value {
		return n.right.Search(value)
	} else if value < n.Value {
		return n.left.Search(value)
	}
	return n
}