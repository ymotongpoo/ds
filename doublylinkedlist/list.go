package doublylinkedlist

type Element struct {
	next  *Element
	prev  *Element
	Value int
}

func (e *Element) Next() *Element {
	return e.next
}

func (e *Element) Prev() *Element {
	return e.prev
}

type List struct {
	first  *Element
	last   *Element
	Length int
}

func NewList() *List {
	return &List{
		first:  nil,
		last:   nil,
		Length: 0,
	}
}

func (l *List) First() *Element {
	return l.first
}

func (l *List) Last() *Element {
	return l.last
}

func (l *List) InsertFirst(e *Element) *Element {
	l.Length++
	if l.first != nil {
		l.first.prev = e
		e.next = l.first
		l.first = e
	} else {
		l.first = e
		l.last = l.first
		e.next = nil
		e.prev = nil
	}
	return e.next
}

func (l *List) InsertLast(e *Element) *Element {
	l.Length++
	if l.last != nil {
		l.last.next = e
		e.prev = l.last
		l.last = e
	} else {
		l.last = e
		l.first = l.last
		e.next = nil
		e.prev = nil
	}
	return e.next
}

func (l *List) DeleteFirst() *Element {
	if l.first == nil {
		return nil
	}
	l.Length--
	deleted := l.first
	l.first = l.first.next
	l.first.prev = nil
	return deleted
}

func (l *List) DeleteLast() *Element {
	if l.last == nil {
		return nil
	}
	l.Length--
	deleted := l.last
	l.last = l.last.prev
	l.last.next = nil
	return deleted
}

func (l *List) InsertAt(n int, e *Element) *Element {
	if n > l.Length-1 {
		return l.InsertLast(e)
	} else if n <= 0 {
		return l.InsertFirst(e)
	}
	l.Length++
	cur := l.first
	for n > 0 {
		cur = cur.next
		n--
	}
	e.prev = cur.prev
	cur.prev.next = e
	e.next = cur
	cur.prev = e
	return e
}

func (l *List) DeleteAt(n int) *Element {
	if n > l.Length-1 {
		return l.DeleteLast()
	} else if n <= 0 {
		return l.DeleteFirst()
	}
	l.Length--
	cur := l.first
	for n > 0 {
		cur = cur.next
		n--
	}
	cur.prev.next = cur.next
	cur.next.prev = cur.prev
	return cur
}
