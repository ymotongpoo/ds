package multilist

import (
	"reflect"
	"testing"
)

func TestListInsertFirst(t *testing.T) {
	l := NewList(2)
	Input := [][]*Element{
		[]*Element{
			l.NewElement(1),
			l.NewElement(2),
			l.NewElement(3),
			l.NewElement(4),
		},
		[]*Element{
			l.NewElement(100),
			l.NewElement(200),
			l.NewElement(300),
		},
	}
	Output1 := [][]int{
		[]int{4, 3, 2, 1},
		[]int{300, 200, 100},
	}
	Output2 := [][]int{
		[]int{1, 2, 3, 4},
		[]int{100, 200, 300},
	}

	out1 := make([][]int, len(Input))
	out2 := make([][]int, len(Input))

	for i, es := range Input {
		for _, e := range es {
			l.InsertFirst(i, e)
		}
		out1[i] = make([]int, len(Input[i]))
		out2[i] = make([]int, len(Input[i]))
	}

	var err error
	for i := 0; i < l.Size(); i++ {
		e1 := l.First(i)
		e2 := l.Last(i)
		for j := 0; j < l.Length(i); j++ {
			out1[i][j] = e1.Value
			out2[i][j] = e2.Value

			e1, err = e1.Next(i)
			if err != nil {
				t.Errorf("%v", err)
			}
			e2, err = e2.Prev(i)
			if err != nil {
				t.Errorf("%v", err)
			}
		}
	}

	if !reflect.DeepEqual(out1, Output1) {
		t.Errorf("forward: out=%v, want %v\n", out1, Output1)
	}
	if !reflect.DeepEqual(out2, Output2) {
		t.Errorf("backward: out=%v, want %v\n", out2, Output2)
	}
	for i := 0; i < l.size; i++ {
		if l.Length(i) != len(Input[i]) {
			t.Errorf("out=%v, want %v\n", l.Length(i), len(Input[i]))
		}
	}
	for i := 0; i < l.size; i++ {
		if l.first[i].prev[i] != l.last[i] {
			t.Errorf("edge is not linked each other")
		}
		if l.last[i].next[i] != l.first[i] {
			t.Errorf("edge is not linked each other")
		}
	}
}

func TestListInsertLast(t *testing.T) {
	l := NewList(2)
	Input := []*Element{
		l.NewElement(1),
		l.NewElement(2),
		l.NewElement(3),
		l.NewElement(4),
		l.NewElement(5),
		l.NewElement(6),
		l.NewElement(7),
		l.NewElement(8),
	}
	Output1 := [][]int{
		[]int{1, 2, 3, 4, 5, 6, 7, 8},
		[]int{1, 3, 5, 7},
	}
	Output2 := [][]int{
		[]int{8, 7, 6, 5, 4, 3, 2, 1},
		[]int{7, 5, 3, 1},
	}

	out1 := make([][]int, 2)
	out2 := make([][]int, 2)
	out1[0] = make([]int, len(Input))
	out2[0] = make([]int, len(Input))
	out1[1] = make([]int, len(Input)/2)
	out2[1] = make([]int, len(Input)/2)

	for i, e := range Input {
		l.InsertLast(0, e)
		if i%2 == 0 {
			l.InsertLast(1, e)
		}
	}

	var err error
	for i := 0; i < l.Size(); i++ {
		e1 := l.First(i)
		e2 := l.Last(i)
		for j := 0; j < l.Length(i); j++ {
			out1[i][j] = e1.Value
			out2[i][j] = e2.Value

			e1, err = e1.Next(i)
			if err != nil {
				t.Errorf("%v", err)
			}
			e2, err = e2.Prev(i)
			if err != nil {
				t.Errorf("%v", err)
			}
		}
	}

	if !reflect.DeepEqual(out1, Output1) {
		t.Errorf("forward: out=%v, want %v\n", out1, Output1)
	}
	if !reflect.DeepEqual(out2, Output2) {
		t.Errorf("backward: out=%v, want %v\n", out2, Output2)
	}
	if l.Length(0) != len(Input) {
		t.Errorf("out=%v, want %v\n", l.Length(0), len(Input))
	}
	if l.Length(1) != len(Input)/2 {
		t.Errorf("out=%v, want %v\n", l.Length(1), len(Input)/2)
	}

	for i := 0; i < l.size; i++ {
		if l.first[i].prev[i] != l.last[i] {
			t.Errorf("edge is not linked each other")
		}
		if l.last[i].next[i] != l.first[i] {
			t.Errorf("edge is not linked each other")
		}
	}
}

func TestListDeleteFirst(t *testing.T) {
	l := NewList(2)
	Input := [][]*Element{
		[]*Element{
			l.NewElement(1),
			l.NewElement(2),
			l.NewElement(3),
			l.NewElement(4),
		},
		[]*Element{
			l.NewElement(100),
			l.NewElement(200),
			l.NewElement(300),
		},
	}
	Input2 := [][]*Element{
		[]*Element{
			l.NewElement(5),
			l.NewElement(6),
			l.NewElement(7),
		},
		[]*Element{
			l.NewElement(400),
			l.NewElement(500),
		},
	}
	Output1 := [][]int{
		[]int{5, 6, 7},
		[]int{400, 500},
	}
	Output2 := [][]int{
		[]int{7, 6, 5},
		[]int{500, 400},
	}

	out1 := make([][]int, len(Input2))
	out2 := make([][]int, len(Input2))

	for i, es := range Input {
		for _, e := range es {
			l.InsertLast(i, e)
		}
	}
	for i, es := range Input2 {
		for _, e := range es {
			l.InsertLast(i, e)
		}
		out1[i] = make([]int, len(Input2[i]))
		out2[i] = make([]int, len(Input2[i]))
	}
	for i, es := range Input {
		for _ = range es {
			l.DeleteFirst(i)
		}
	}

	var err error
	for i := 0; i < l.Size(); i++ {
		e1 := l.First(i)
		e2 := l.Last(i)
		for j := 0; j < l.Length(i); j++ {
			out1[i][j] = e1.Value
			out2[i][j] = e2.Value

			e1, err = e1.Next(i)
			if err != nil {
				t.Errorf("%v", err)
			}
			e2, err = e2.Prev(i)
			if err != nil {
				t.Errorf("%v", err)
			}
		}
	}

	if !reflect.DeepEqual(out1, Output1) {
		t.Errorf("forward: out=%v, want %v\n", out1, Output1)
	}
	if !reflect.DeepEqual(out2, Output2) {
		t.Errorf("backward: out=%v, want %v\n", out2, Output2)
	}
	for i := 0; i < l.size; i++ {
		if l.Length(i) != len(Input2[i]) {
			t.Errorf("out=%v, want %v\n", l.Length(i), len(Input2[i]))
		}
	}
	for i := 0; i < l.size; i++ {
		if l.first[i].prev[i] != l.last[i] {
			t.Errorf("edge is not linked each other")
		}
		if l.last[i].next[i] != l.first[i] {
			t.Errorf("edge is not linked each other")
		}
	}
}

func TestListDeleteLast(t *testing.T) {
	l := NewList(2)
	Input := []*Element{
		l.NewElement(1),
		l.NewElement(2),
		l.NewElement(3),
		l.NewElement(4),
		l.NewElement(5),
		l.NewElement(6),
		l.NewElement(7),
		l.NewElement(8),
	}
	Output1 := [][]int{
		[]int{1, 2, 3, 4, 5, 6},
		[]int{1, 3, 5, 7},
	}
	Output2 := [][]int{
		[]int{6, 5, 4, 3, 2, 1},
		[]int{7, 5, 3, 1},
	}

	out1 := make([][]int, 2)
	out2 := make([][]int, 2)
	out1[0] = make([]int, len(Input)-2)
	out2[0] = make([]int, len(Input)-2)
	out1[1] = make([]int, len(Input)/2)
	out2[1] = make([]int, len(Input)/2)

	for i, e := range Input {
		l.InsertLast(0, e)
		if i%2 == 0 {
			l.InsertLast(1, e)
		}
	}
	l.DeleteLast(0) // Deleting 8
	l.DeleteLast(0) // Deleting 7

	var err error
	for i := 0; i < l.Size(); i++ {
		e1 := l.First(i)
		e2 := l.Last(i)
		for j := 0; j < l.Length(i); j++ {
			out1[i][j] = e1.Value
			out2[i][j] = e2.Value

			e1, err = e1.Next(i)
			if err != nil {
				t.Errorf("%v", err)
			}
			e2, err = e2.Prev(i)
			if err != nil {
				t.Errorf("%v", err)
			}
		}
	}

	if !reflect.DeepEqual(out1, Output1) {
		t.Errorf("forward: out=%v, want %v\n", out1, Output1)
	}
	if !reflect.DeepEqual(out2, Output2) {
		t.Errorf("backward: out=%v, want %v\n", out2, Output2)
	}
	if l.Length(0) != len(Input)-2 {
		t.Errorf("out=%v, want %v\n", l.Length(0), len(Input)-2)
	}
	if l.Length(1) != len(Input)/2 {
		t.Errorf("out=%v, want %v\n", l.Length(1), len(Input)/2)
	}

	for i := 0; i < l.size; i++ {
		if l.first[i].prev[i] != l.last[i] {
			t.Errorf("edge is not linked each other")
		}
		if l.last[i].next[i] != l.first[i] {
			t.Errorf("edge is not linked each other")
		}
	}
}
