package multilist

import "errors"

var ErrOutOfRange = errors.New("Out of range")

// Element is a struct which express element in a list.
type Element struct {
	next  []*Element // next elements in each lists
	prev  []*Element // previous elements in each lists
	Value int        // value of element
}

// Next returns next element in nth list.
func (e *Element) Next(n int) (*Element, error) {
	if n < 0 || n >= len(e.next) {
		return nil, ErrOutOfRange
	}
	return e.next[n], nil
}

// Prev returns previous element in nth list.
func (e *Element) Prev(n int) (*Element, error) {
	if n < 0 || n >= len(e.prev) {
		return nil, ErrOutOfRange
	}
	return e.prev[n], nil
}

// List is multi dimensioned ring list.
type List struct {
	size   int        // dimension of multi list
	first  []*Element // first elements of each lists
	last   []*Element // last elements of each lists
	length []int      // length of each lists
}

// NewList returns pointer of n-dimension List.
func NewList(n int) *List {
	return &List{
		size:   n,
		first:  make([]*Element, n),
		last:   make([]*Element, n),
		length: make([]int, n),
	}
}

// NewElement returns pointer of Element for multi list l.
func (l *List) NewElement(v int) *Element {
	return &Element{
		next:  make([]*Element, l.size),
		prev:  make([]*Element, l.size),
		Value: v,
	}
}

// First returns first element of nth list l.
func (l *List) First(n int) *Element {
	if n < 0 || n >= l.size {
		return nil
	}
	return l.first[n]
}

// Last returns last element of nth list l.
func (l *List) Last(n int) *Element {
	if n < 0 || n >= l.size {
		return nil
	}
	return l.last[n]
}

// Length returns length of nth list.
func (l *List) Length(n int) int {
	if n < 0 || n >= l.size {
		return 0

	}
	return l.length[n]
}

// Size returns dimension of l.
func (l *List) Size() int {
	return l.size
}

// InsertFirst inserts Element e as first of nth list.
func (l *List) InsertFirst(n int, e *Element) error {
	if n < 0 || n >= l.size {
		return ErrOutOfRange
	}
	if l.first[n] == nil {
		l.first[n] = e
		l.last[n] = l.first[n]
		e.next[n] = e
		e.prev[n] = e
	} else {
		e.next[n] = l.first[n]
		e.prev[n] = l.last[n]
		l.first[n].prev[n] = e
		l.last[n].next[n] = e
		l.first[n] = e
	}
	l.length[n]++
	return nil
}

// InsertLast inserts Element e as last nth list.
func (l *List) InsertLast(n int, e *Element) error {
	if n < 0 || n >= l.size {
		return ErrOutOfRange
	}
	if l.last[n] == nil {
		l.last[n] = e
		l.first[n] = e
		e.next[n] = e
		e.prev[n] = e
	} else {
		e.prev[n] = l.last[n]
		e.next[n] = l.first[n]
		l.last[n].next[n] = e
		l.first[n].prev[n] = e
		l.last[n] = e
	}
	l.length[n]++
	return nil
}

// DeleteFirst delete first element of nth list.
func (l *List) DeleteFirst(n int) (*Element, error) {
	if n < 0 || n >= l.size {
		return nil, ErrOutOfRange
	}
	ret := l.first[n]
	if l.first[n] == nil {
		return nil, nil
	} else {
		l.last[n].next[n] = l.first[n].next[n]
		l.first[n].next[n].prev[n] = l.last[n]
		l.first[n] = l.first[n].next[n]
	}
	l.length[n]--
	return ret, nil
}

// DeleteLast delete last element of nth list.
func (l *List) DeleteLast(n int) (*Element, error) {
	if n < 0 || n >= l.size {
		return nil, ErrOutOfRange
	}
	ret := l.last[n]
	if l.last[n] == nil {
		return nil, nil
	} else {
		l.first[n].prev[n] = l.last[n].prev[n]
		l.last[n].prev[n].next[n] = l.first[n]
		l.last[n] = l.last[n].prev[n]
	}
	l.length[n]--
	return ret, nil
}

// InsertAt inserts Element e at position n of dim-th list.
func (l *List) InsertAt(dim int, n int, e *Element) error {
	if dim < 0 || dim >= l.size {
		return ErrOutOfRange
	}
	cur := l.first[dim]
	if n >= 0 {
		if n%l.length[dim] == 0 {
			return l.InsertFirst(dim, e)
		} else if n%l.length[dim] == l.length[dim]-1 {
			return l.InsertLast(dim, e)
		}
		for n > 0 {
			cur = cur.next[dim]
			n--
		}
	} else {
		if n%l.length[dim] == -1 {
			return l.InsertLast(dim, e)
		} else if n%l.length[dim] == 0 {
			return l.InsertFirst(dim, e)
		}
		for n < 0 {
			cur = cur.next[dim]
			n++
		}
	}
	l.length[dim]++
	e.prev[dim] = cur.prev[dim]
	cur.prev[dim].next[dim] = e
	e.next[dim] = cur
	cur.prev[dim] = e
	return nil
}

// DeleteAt deletes an element at position n of dim-th list.
func (l *List) DeleteAt(dim, n int) (*Element, error) {
	if dim < 0 || dim >= l.size {
		return nil, ErrOutOfRange
	}
	cur := l.first[dim]
	if n > 0 {
		if n%l.length[dim] == 0 {
			return l.DeleteFirst(dim)
		} else if n%l.length[dim] == l.length[dim]-1 {
			return l.DeleteLast(dim)
		}
		for n > 0 {
			cur = cur.next[dim]
			n--
		}
	} else {
		if n%l.length[dim] == -1 {
			return l.DeleteLast(dim)
		} else if n%l.length[dim] == 0 {
			return l.DeleteFirst(dim)
		}
		for n < 0 {
			cur = cur.next[dim]
			n++
		}
	}
	l.length[dim]--
	cur.prev[dim].next[dim] = cur.next[dim]
	cur.next[dim].prev[dim] = cur.prev[dim]
	return cur, nil
}
