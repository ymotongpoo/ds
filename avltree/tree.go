package avltree

import "errors"

type Balance int

const (
	Equal Balance = iota
	LeftHeavy
	RightHeavy
)

type Node struct {
	parent *Node
	left *Node
	right *Node
	balance Balance
	Value int
}

func NewNode(value int) *Node {
	return &Node {
		balance: Equal,
		Value: value,
	}
}

func (n *Node) Left() *Node {
	return n.left
}

func (n *Node) Right() *Node {
	return n.right
}

func (n *Node) Parent() *Node {
	return n.parent
}

func (n *Node) add(value int) *Node {
	var node *Node
	if value > n.Value {
		node = n.right
	} else {
		node = n.left
	}
	if node != nil {
		return node.Add(value)
	}
	node = NewNode(value)
	node.parent = n
	return node
}

func (n *Node)rotateLeft() *Node {
	top = n.right
	tl = top.left
	top.left = n
	n.right = tl
	n.balance--
}