package linkedlist

// Element 
type Element struct {
	next  *Element
	Value int
}

func (e *Element) Next() *Element {
	return e.next
}

// List
type List struct {
	first    *Element
	last     *Element
	length   int
}

// NewList returns pointer of initialized List.
func NewList() *List {
	l := &List{}
	l.first = l.last
	return l
}

// First returns first element of List l.
func (l *List) First() *Element {
	return l.first
}

// Last returns last element of List l.
func (l *List) Last() *Element {
	return l.last
}

// Length returns length of List l.
func (l *List) Length() int {
	return l.length
}

// InsertFist inserts e as first element of l.
// It returns next element's pointer of inserted position.
func (l *List) InsertFirst(e *Element) *Element {
	l.length++
	if l.first == nil {
		l.first = e
		l.last = l.first
		e.next = nil
	} else {
		e.next = l.first
		l.first = e
	}
	return e.next
}

// InsertFirst inserts e as last element of l.
// It returns last element's pointer.
func (l *List) InsertLast(e *Element) *Element {
	l.length++
	if l.last == nil {
		l.last = e
		return e
	}
	if l.first == nil {
		l.first = l.last
	}
	l.last.next = e
	l.last = e
	return e
}

// DeleteFirst deletes first element of l.
// It returns a pointer of deleted element. (same as pop)
func (l *List) DeleteFirst() *Element {
	l.length--
	ret := l.first
	if l.first == nil {
		return nil
	}
	if l.first == l.last {
		l.last = l.last.next
	}
	l.first = l.first.next
	return ret
}

// DeleteLast deletes last element of l.
// It returns a pointer of deleted element. (same as pop)
func (l *List) DeleteLast() *Element {
	l.length--
	ret := l.last
	var secondLast *Element
	if l.last == l.first {
		l.last = nil
		l.first = nil
		return ret
	}
	for e := l.first; e != nil; e = e.next {
		if e.next == l.last {
			secondLast = e
			break
		}
	}
	l.last = secondLast
	secondLast.next = nil
	return ret
}

// InsertAt inserts an element at position n of l.
// It returns a pointer of inserted element.
func (l *List) InsertAt(n int, e *Element) *Element {
	if n > l.length {
		return l.InsertLast(e)
	} else if n <= 0 {
		return l.InsertFirst(e)
	}
	l.length++
	prev := l.first
	for i := 0; i < n-1; i++ {
		prev = prev.next
	}
	e.next = prev.next
	prev.next = e
	return e
}

// DeleteAt deletes an elements at position n of l.
// It returns a pointer of deleted elelment.
func (l *List) DeleteAt(n int) *Element {
	if n > l.length {
		return l.DeleteLast()
	} else if n <= 0 {
		return l.DeleteFirst()
	}
	l.length--
	prev := l.first
	for i := 0; i < n-1; i++ {
		prev = prev.next
	}
	deleted := prev.next
	prev.next = prev.next.next
	return deleted
}
