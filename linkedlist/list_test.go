package linkedlist

import (
	"reflect"
	"testing"
)

func TestListInsertFirst(t *testing.T) {
	Input := []*Element{
		&Element{Value: 1},
		&Element{Value: 2},
		&Element{Value: 3},
	}
	Output := []int{3, 2, 1}

	l := NewList()
	for _, e := range Input {
		l.InsertFirst(e)
	}

	out := make([]int, len(Input))
	var index int
	for e := l.First(); e != nil; e = e.Next() {
		out[index] = e.Value
		index++
	}

	if !reflect.DeepEqual(out, Output) {
		t.Errorf("out=%v, want %v\n", out, Output)
	}
}

func TestListInsertLast(t *testing.T) {
	Input := []*Element{
		&Element{Value: 1},
		&Element{Value: 2},
		&Element{Value: 3},
	}
	Output := []int{1, 2, 3}

	l := NewList()
	for _, e := range Input {
		l.InsertLast(e)
	}

	out := make([]int, len(Input))
	var index int
	for e := l.First(); e != nil; e = e.Next() {
		out[index] = e.Value
		index++
	}

	if !reflect.DeepEqual(out, Output) {
		t.Errorf("out=%v, want %v\n", out, Output)
	}
}

func TestListDeleteFirst(t *testing.T) {
	Input := []*Element{
		&Element{Value: 1},
		&Element{Value: 2},
		&Element{Value: 3},
	}

	l := NewList()
	for _, e := range Input {
		l.InsertLast(e)
	}

	for i := 0; i < len(Input); i++ {
		l.DeleteFirst()
	}

	if !reflect.DeepEqual(l, NewList()) {
		t.Errorf("out=%v, want %v\n", l, NewList())
	}
}

func TestListDeleteLast(t *testing.T) {
	Input := []*Element{
		&Element{Value: 1},
		&Element{Value: 2},
		&Element{Value: 3},
	}

	l := NewList()
	for _, e := range Input {
		l.InsertFirst(e)
	}

	for i := 0; i < len(Input); i++ {
		l.DeleteLast()
	}

	if !reflect.DeepEqual(l, NewList()) {
		t.Errorf("out=%v, want %v\n", l, NewList())
	}
}

func TestListInsertAt(t *testing.T) {
	Input := []*Element{
		&Element{Value: 1},
		&Element{Value: 2},
		&Element{Value: 3},
		&Element{Value: 4},
		&Element{Value: 5},
	}
	Input2 := &Element{Value: 100}
	Output := []int{1, 2, 3, 100, 4, 5}

	l := NewList()
	for _, e := range Input {
		l.InsertLast(e)
	}
	l.InsertAt(3, Input2)

	out := make([]int, len(Input)+1)
	var index int
	for e := l.First(); e != nil; e = e.Next() {
		out[index] = e.Value
		index++
	}

	if !reflect.DeepEqual(out, Output) {
		t.Errorf("out=%v, want %v\n", out, Output)
	}
	if l.Length() != len(Input)+1 {
		t.Errorf("out=%v, want %v\n", l.Length(), len(Input)+1)
	}
}

func TestListDeleteAt(t *testing.T) {
	Input := []*Element{
		&Element{Value: 1},
		&Element{Value: 2},
		&Element{Value: 3},
		&Element{Value: 100},
		&Element{Value: 4},
		&Element{Value: 5},
	}
	Output := []int{1, 2, 3, 4, 5}

	l := NewList()
	for _, e := range Input {
		l.InsertLast(e)
	}
	l.DeleteAt(3)
	out := make([]int, len(Input)-1)
	var index int
	for e := l.First(); e != nil; e = e.Next() {
		out[index] = e.Value
		index++
	}

	if !reflect.DeepEqual(out, Output) {
		t.Errorf("out=%v, want %v\n", out, Output)
	}
	if l.Length() != len(Input)-1 {
		t.Errorf("out=%v, want %v\n", l.Length(), len(Input)-1)
	}
}
