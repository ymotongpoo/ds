#!/bin/bash

root=$PWD
for dir in * ; do
	if [ -d "$dir" ] ; then
        echo "========== $dir"
		cd "$dir"
		go test || exit 1
		cd ..
	fi
done
